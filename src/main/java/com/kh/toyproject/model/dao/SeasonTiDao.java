package com.kh.toyproject.model.dao;

import org.springframework.data.repository.CrudRepository;

import com.kh.toyproject.model.vo.SeasonTi;

public interface SeasonTiDao extends CrudRepository<SeasonTi, String>{

}
