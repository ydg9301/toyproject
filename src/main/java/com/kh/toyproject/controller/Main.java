package com.kh.toyproject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.kh.toyproject.model.dao.MemberDao;
import com.kh.toyproject.model.dao.SeasonTiDao;
import com.kh.toyproject.model.vo.SeasonTi;

@Controller
public class Main {
	
	@Autowired
	private MemberDao md;
	
	@Autowired
	private SeasonTiDao st;
	
	@GetMapping("/index")
	public void TestMain(Model model) {
		System.out.println("한번 해봅시다ㅏ");
		
	 System.out.println("me 의 값 : " +md.findAll());
	 
	 SeasonTi ti = new SeasonTi();
	 
	 
	 ti.setSeasonDate("테스트");
	 ti.setSeasonDc("테스트!");
	 ti.setSeasonId(4);
	 ti.setSeasonMoney("테스트 !");
	 ti.setSeasonName("민경현");
	 
	 st.save(ti);
	 
	 model.addAttribute("md" , md.findAll());
		
		
	}

}
